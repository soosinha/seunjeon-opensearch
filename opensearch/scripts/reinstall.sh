#!/bin/bash

PLUGIN_VERSION=1.0.0.0

script_dir=$(cd "$(dirname "$0")" && pwd)

opensearch_path=~/Programs/opensearch
plugin_bin=bin/opensearch-plugin
${opensearch_path}/${plugin_bin} list
${opensearch_path}/${plugin_bin} remove analysis-seunjeon
${opensearch_path}/${plugin_bin} list
${opensearch_path}/${plugin_bin} install file://${script_dir}/../target/opensearch-analysis-seunjeon-assembly-${PLUGIN_VERSION}.zip
${opensearch_path}/${plugin_bin} list
cp user_dict.csv ${opensearch_path}/config
ls -al ${opensearch_path}/config/user_dict.csv

