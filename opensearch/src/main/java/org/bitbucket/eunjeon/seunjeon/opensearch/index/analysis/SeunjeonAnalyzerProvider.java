package org.bitbucket.eunjeon.seunjeon.opensearch.index.analysis;

import org.bitbucket.eunjeon.seunjeon.opensearch.SeunjeonAnalyzer;
import org.opensearch.common.settings.Settings;
import org.opensearch.env.Environment;
import org.opensearch.index.IndexSettings;
import org.opensearch.index.analysis.AbstractIndexAnalyzerProvider;


public class SeunjeonAnalyzerProvider extends AbstractIndexAnalyzerProvider<SeunjeonAnalyzer> {
    private final SeunjeonAnalyzer analyzer;

    public SeunjeonAnalyzerProvider(IndexSettings indexSettings, Environment environment, String name, Settings settings) {
        super(indexSettings, name, settings);
        analyzer = new SeunjeonAnalyzer();
    }

    @Override
    public SeunjeonAnalyzer get() {
        return analyzer;
    }

}
