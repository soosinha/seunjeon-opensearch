package org.bitbucket.eunjeon.seunjeon.opensearch.plugin.analysis;

import org.apache.lucene.analysis.Analyzer;
import org.bitbucket.eunjeon.seunjeon.opensearch.index.analysis.SeunjeonAnalyzerProvider;
import org.bitbucket.eunjeon.seunjeon.opensearch.index.analysis.SeunjeonTokenizerFactory;
import org.opensearch.index.analysis.AnalyzerProvider;
import org.opensearch.index.analysis.TokenizerFactory;
import org.opensearch.indices.analysis.AnalysisModule;
import org.opensearch.plugins.AnalysisPlugin;
import org.opensearch.plugins.Plugin;

import java.util.Map;

import static java.util.Collections.singletonMap;

public class AnalysisSeunjeonPlugin extends Plugin implements AnalysisPlugin {

    @Override
    public Map<String, AnalysisModule.AnalysisProvider<TokenizerFactory>> getTokenizers() {
        return singletonMap("seunjeon_tokenizer", SeunjeonTokenizerFactory::new);
    }

    @Override
    public Map<String, AnalysisModule.AnalysisProvider<AnalyzerProvider<? extends Analyzer>>> getAnalyzers() {
        return singletonMap("seunjeon_analyzer", SeunjeonAnalyzerProvider::new);
    }

}
