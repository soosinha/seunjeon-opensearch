package org.bitbucket.eunjeon.seunjeon.opensearch;

/*
import org.bitbucket.eunjeon.seunjeon.opensearch.plugin.analysis.AnalysisSeunjeonPlugin;
import org.opensearch.Version;
import org.opensearch.cluster.metadata.IndexMetaData;
import org.opensearch.common.compress.CompressedXContent;
import org.opensearch.common.settings.Settings;
import org.opensearch.common.xcontent.XContentFactory;
import org.opensearch.env.Environment;
import org.opensearch.index.IndexService;
import org.opensearch.index.mapper.DocumentMapper;
import org.opensearch.index.mapper.DocumentMapperParser;
import org.opensearch.plugins.Plugin;
import org.opensearch.test.ESSingleNodeTestCase;
import org.opensearch.test.InternalSettingsPlugin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;


public class NodeTest extends ESSingleNodeTestCase {
    @Override
    protected Collection<Class<? extends Plugin>> getPlugins() {
        return pluginList(InternalSettingsPlugin.class, AnalysisSeunjeonPlugin.class);
    }

    protected Settings nodeSettings() {
        Path home = createTempDir();
        Path config = home.resolve("config");
        try {
            Files.createDirectory(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Settings.builder().put(Environment.PATH_HOME_SETTING.getKey(), home).build();
    }

    public void testIndex() throws IOException {
        String json = "/org/bitbucket/eunjeon/seunjeon/opensearch/seunjeon_analysis.json";

        Settings settings = Settings.builder()
                .loadFromStream(json, NodeTest.class.getResourceAsStream(json))
                .put(IndexMetaData.SETTING_VERSION_CREATED, Version.CURRENT)
                .build();

        IndexService indexService = createIndex("seunjeon-idx", settings);
        DocumentMapperParser mapperParser = indexService.mapperService().documentMapperParser();
//        DocumentMapper mapper = mapperParser.parse("type", new CompressedXContent(mapping.string()));

        String mapping = XContentFactory.jsonBuilder().startObject().startObject("type").startObject("properties")
                .startObject("foo").field("enabled", false).endObject()
                .startObject("bar").field("type", "integer").endObject()
                .endObject().endObject().endObject().string();

        System.out.println(mapping);



    }
}
*/
